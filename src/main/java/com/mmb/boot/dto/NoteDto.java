package com.mmb.boot.dto;

import org.dozer.Mapping;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NoteDto {
    @JsonProperty
    @Mapping(value = "id")
    public Long id;

    @JsonProperty
    @Mapping
    public String string;

    @JsonProperty
    @Mapping
    public String string2;

    @JsonProperty
    @Mapping
    public Long version;
}
