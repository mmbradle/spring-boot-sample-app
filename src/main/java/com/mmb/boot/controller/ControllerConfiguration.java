package com.mmb.boot.controller;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmb.boot.service.ServiceConfiguration;
import com.mmb.boot.util.UtilConfiguration;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = ControllerConfiguration.class)
@Import({ ServiceConfiguration.class, UtilConfiguration.class })
public class ControllerConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(this.objectMapper);
        converters.add(converter);
    }

    @Bean
    Mapper mapper() {
        return new DozerBeanMapper();
    }

}