package com.mmb.boot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * A small controller that gives a heart-beat message
 */
@RestController
@RequestMapping(value = "/status")
public class StatusController implements Controller {

    private final String heartBeatMessage = "hi";

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String get() {
        return this.heartBeatMessage;
    }

}