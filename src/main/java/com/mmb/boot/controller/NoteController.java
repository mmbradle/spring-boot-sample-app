package com.mmb.boot.controller;

import java.util.Optional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mmb.boot.dto.NoteDto;
import com.mmb.boot.model.NotePojo;
import com.mmb.boot.service.NoteService;

// TODO: Handle Validation

@RestController
@RequestMapping(value = "/notes")
public class NoteController implements Controller {

    @Autowired
    private NoteService noteService;

    @Autowired
    Mapper mapper;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public NoteDto get(@PathVariable final Long id) {
        Optional<NotePojo> note = this.noteService.getNote(id);
        if (note.isPresent()) {
            return this.mapper.map(note.get(), NoteDto.class);
        } else {
            //            throw new ResourceNotFoundException(); TODO FIX
            throw new RuntimeException();
        }

    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(value = HttpStatus.CREATED)
    public NoteDto post(@RequestBody final NoteDto noteDto) {
        NotePojo pojo = this.mapper.map(noteDto, NotePojo.class);
        pojo = this.noteService.createNote(pojo);
        NoteDto dto = this.mapper.map(pojo, NoteDto.class);
        return dto;
    }

}