package com.mmb.boot.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import com.mmb.boot.controller.ControllerConfiguration;

@Configuration
@ComponentScan(basePackageClasses = ApplicationConfiguration.class)
@Import({ ControllerConfiguration.class })
public class ApplicationConfiguration {

    public static final String MAIN_PROFILE = "prod";
    public static final String TEST_PROFILE = "test";

    /**
     * These static classes are required because it makes it possible to use different properties files for every Spring
     * profile. See: <a href="http://stackoverflow.com/a/14167357/313554">This StackOverflow answer</a> for more
     * details.
     */
    @Profile(MAIN_PROFILE)
    @Configuration
    @PropertySource("classpath:application.properties")
    static class ApplicationProperties {
    }

    @Profile(TEST_PROFILE)
    @Configuration
    @PropertySource("classpath:integration-test.properties")
    // TODO: Use Nate's method of overriding properties files.
    static class IntegrationTestProperties {
    }

}