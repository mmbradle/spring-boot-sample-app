package com.mmb.boot.application;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ApplicationUtils {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Mapper mapper;

    public <POJO> String pojoToJson(final POJO pojo, final Class<?> dtoClass) throws JsonProcessingException {
        String jsonIn = this.objectMapper.writeValueAsString(this.mapper.map(pojo, dtoClass));
        return jsonIn;
    }

}
