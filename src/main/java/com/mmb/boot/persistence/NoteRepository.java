package com.mmb.boot.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.Repository;

import com.mmb.boot.model.NotePojo;

public interface NoteRepository extends Repository<NotePojo, Long> {
    void delete(NotePojo deleted);

    List<NotePojo> findAll();

    Optional<NotePojo> findOne(Long id);

    NotePojo save(NotePojo persisted);
}
