package com.mmb.boot.util;

import java.io.IOException;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@Component
@Primary
public class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        //@formatter:off
       setVisibilityChecker(getSerializationConfig().getDefaultVisibilityChecker()
               .withFieldVisibility(Visibility.NONE)
               .withGetterVisibility(Visibility.NONE)
               .withIsGetterVisibility(Visibility.NONE)
               .withSetterVisibility(Visibility.NONE)
               .withCreatorVisibility(Visibility.NONE));
       //@formatter:on

        registerModule(new SimpleModule().addSerializer(String.class, new StringJsonSerializer()));
        registerModule(new SimpleModule().addDeserializer(String.class, new StringJsonDeserializer()));
    }

    private static class StringJsonSerializer extends StdSerializer<String> {

        public StringJsonSerializer() {
            super(String.class);
        }

        @Override
        public void serialize(final String value, final JsonGenerator jgen, final SerializerProvider provider)
                throws IOException, JsonGenerationException {
            jgen.writeString(value.toUpperCase());
        }
    }

    private static class StringJsonDeserializer extends StdDeserializer<String> {

        public StringJsonDeserializer() {
            super(String.class);
        }

        @Override
        public String deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException,
                JsonProcessingException {

            return jp.getText().toLowerCase();
        }

    }

}
