package com.mmb.boot.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = UtilConfiguration.class)
public class UtilConfiguration {
}