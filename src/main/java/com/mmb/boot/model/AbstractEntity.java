package com.mmb.boot.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public class AbstractEntity {

    private Long id;

    private String string;

    private Long version;

    public AbstractEntity() {
    }

    public AbstractEntity(final Long id, final String string) {
        this.id = id;
        this.string = string;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = "string", nullable = false, length = 100)
    public String getString() {
        return this.string;
    }

    public void setString(final String string) {
        this.string = string;
    }

    @Version
    public Long getVersion() {
        return this.version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public void applyChanges(final AbstractEntity inAbstractEntity) {
        this.setString(inAbstractEntity.getString());
    }

}
