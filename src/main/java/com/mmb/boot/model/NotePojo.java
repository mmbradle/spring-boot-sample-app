package com.mmb.boot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "NOTE")
public class NotePojo extends AbstractEntity {

    private String string2;

    public NotePojo() {
        super();
    }

    public NotePojo(final Long id, final String string, final String string2) {
        super(id, string);
        this.string2 = string2;
    }

    @Column(name = "string2", nullable = false, length = 100)
    public String getString2() {
        return this.string2;
    }

    public void setString2(final String string2) {
        this.string2 = string2;
    }

    public void applyChanges(final NotePojo inNotePojo) {
        this.setString2(inNotePojo.getString2());
        super.applyChanges(inNotePojo);
    }

}
