package com.mmb.boot.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mmb.boot.model.NotePojo;
import com.mmb.boot.persistence.NoteRepository;

@Component
public class NoteService {
    @Autowired
    NoteRepository noteRepository;

    public Optional<NotePojo> getNote(final Long id) {
        return this.noteRepository.findOne(id);
    }

    public NotePojo createNote(final NotePojo notePojo) {
        return this.noteRepository.save(notePojo);
    }

}
