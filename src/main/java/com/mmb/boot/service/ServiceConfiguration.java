package com.mmb.boot.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.mmb.boot.persistence.PersistenceConfiguration;

@Configuration
@ComponentScan(basePackageClasses = ServiceConfiguration.class)
@Import({ PersistenceConfiguration.class })
public class ServiceConfiguration {
}