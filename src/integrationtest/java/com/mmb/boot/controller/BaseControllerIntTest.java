package com.mmb.boot.controller;

import org.dozer.Mapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
abstract public class BaseControllerIntTest {

    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected Mapper mapper;

    @Autowired
    protected ObjectMapper objectMapper;

    @Before
    public void beforeBaseControllerIntTest() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

}
