package com.mmb.boot.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.mmb.boot.application.ApplicationConfiguration;
import com.mmb.boot.dto.NoteDto;
import com.mmb.boot.model.NotePojo;

@ActiveProfiles(ApplicationConfiguration.TEST_PROFILE)
@ContextConfiguration(classes = { ApplicationConfiguration.class })
public class NoteControllerIntTest extends BaseControllerIntTest {

    @Autowired
    private NoteController noteController;

    @Test
    public void testCrud() throws Exception {
        // Setup
        NotePojo pojo = new NotePojo(null, "note", "note2");
        String jsonIn = this.objectMapper.writeValueAsString(this.mapper.map(pojo, NoteDto.class));
        System.out.println(jsonIn);
        // Insert
        //@formatter:off
        MvcResult result = this.mockMvc.perform(
            post("/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(jsonIn ))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(jsonPath("$.id", Matchers.notNullValue()))
            .andExpect(jsonPath("$.version", Matchers.notNullValue()))
            .andExpect(jsonPath("$.string", is("NOTE")))
            .andReturn();
        //@formatter:on

        NoteDto noteDto = this.objectMapper.readValue(result.getResponse().getContentAsString(), NoteDto.class);
        int id = noteDto.id.intValue();
        int version = noteDto.version.intValue();

        // Get
        //@formatter:off
        this.mockMvc.perform(
            get("/notes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(id)))
            .andExpect(jsonPath("$.version", is(version)))
            .andExpect(jsonPath("$.string", is("NOTE")));
        //@formatter:on
    }

}