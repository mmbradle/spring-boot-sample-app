package com.mmb.boot.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.mmb.boot.dto.NoteDto;
import com.mmb.boot.model.NotePojo;
import com.mmb.boot.service.NoteService;

public class NoteControllerTest extends BaseControllerTest {

    @InjectMocks
    private NoteController noteController;

    @Mock
    private NoteService noteService;

    @Override
    protected Controller getControllerUnderTest() {
        return this.noteController;
    }

    @Test
    public void testGet() throws Exception {
        NotePojo pojo = new NotePojo(123L, "note", "note2");
        when(this.noteService.getNote(pojo.getId())).thenReturn(Optional.of(pojo));

        //@formatter:off
        this.mockMvc.perform(
            get("/notes/123")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk())
            .andExpect(content().string(pojoToJson(pojo, NoteDto.class)));
        //@formatter:on

        verify(this.noteService).getNote(pojo.getId());
        verifyNoMoreInteractions(this.noteService);
    }

    @Test
    public void testPost() throws Exception {
        NotePojo pojo = new NotePojo(null, "note", "note2");
        NotePojo pojoWithId = new NotePojo(0L, "note", "note2");
        when(this.noteService.createNote(Mockito.any(NotePojo.class))).thenReturn(pojoWithId);

        //@formatter:off
        this.mockMvc.perform(
                post("/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(pojoToJson(pojo, NoteDto.class)))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isCreated())
            .andExpect(content().string(pojoToJson(pojoWithId, NoteDto.class)));
        //@formatter:on

        verify(this.noteService).createNote(Mockito.any(NotePojo.class));
        verifyNoMoreInteractions(this.noteService);
    }
}