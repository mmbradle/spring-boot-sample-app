package com.mmb.boot.controller;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmb.boot.util.CustomObjectMapper;

abstract public class BaseControllerTest {

    protected MockMvc mockMvc;

    protected ObjectMapper objectMapper;

    // It is expected that each controller under test will have a Dozer Mapper. This is by convention not contract.
    // This next line will get each test access to a Dozer Mapper, likely there is a better way to do this, or test should access a different Mapper.
    @Spy
    protected final Mapper mapper = new DozerBeanMapper();

    @Before
    public void beforeBaseControllerTest() {
        this.mockMvc = null;
        this.objectMapper = new CustomObjectMapper();

        MockitoAnnotations.initMocks(this);

        this.objectMapper = new CustomObjectMapper();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(this.objectMapper); // TODO get this as a bean?

        this.mockMvc = MockMvcBuilders.standaloneSetup(getControllerUnderTest()).setMessageConverters(converter)
                .build();

    }

    protected abstract Controller getControllerUnderTest();

    protected <POJO> String pojoToJson(final POJO pojo, final Class<?> dtoClass) throws JsonProcessingException {
        String jsonIn = this.objectMapper.writeValueAsString(this.mapper.map(pojo, dtoClass));
        return jsonIn;
    }

}
