package com.mmb.boot.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

public class StatusControllerTest extends BaseControllerTest {

    @InjectMocks
    private StatusController statusController;

    @Override
    protected Controller getControllerUnderTest() {
        return this.statusController;
    }

    @Test
    public void testGet() throws Exception {

        //@formatter:off
        this.mockMvc.perform(
            get("/status")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk())
            .andExpect(content().string("\"HI\""));
        //@formatter:on

    }

}